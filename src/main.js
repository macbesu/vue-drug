// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import "babel-polyfill"
import Vue from 'vue'
import VueRouter from 'vue-router'
import routes from './publicAPI/routes'
import './assets/reset.css'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-default/index.css'
import store from './store/'

Vue.use(VueRouter)
/* eslint-disable no-new */
Vue.use(ElementUI)

const router = new VueRouter({
  routes
})

// router.beforeEach((to, from, next) => {
//     if(localStorage.getItem('account')){
//         alert(1);
//         next();
//     }else{
//         alert(2);
//         router.push({ path:"/" });
//     }
// })
router.beforeEach(({meta, path}, from, next) => {
    var { auth = true } = meta
    // console.log(JSON.stringify(meta));
    // console.warn(JSON.stringify(path));
    var isLogin;
    if(store.state.user.user){
      isLogin = true; //true用户已登录， false用户未登录
    } else {
      isLogin = false;
    }

    if (auth && !isLogin && path !== '/' ) {
        return next({ path: '/' })
    } else if (auth && isLogin && path === '/') {
        return next({ path: '/home' })
    }

    next()
})


new Vue ({
    router
}).$mount('#app');
