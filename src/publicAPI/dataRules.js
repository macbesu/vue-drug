
export default {
    serDrugRule: function(rangeStr, start, end) { //添加药品范围
        if((0<=parseInt(start))
            && (parseInt(start)<=2048)
            && (0<=parseInt(end))
            && (parseInt(end)<=2048)
            && (parseInt(start)<=parseInt(end))
            && (rangeStr!="")
            && (start!="")
            && (end!=""))
            return true;
        else
            return false;
    },

    formatDate: function(date) {
        if(!!!date) {
          return "";
        }
        if(typeof(date)!='string'){
            var yy = date.getFullYear();
            var MM = date.getMonth() + 1;
            MM = MM < 10 ? ('0' + MM) : MM;
            var dd = date.getDate();
            dd = dd < 10 ? ('0' + dd) : dd;
            // console.warn(yy + '-' + MM + '-' + dd)
            return yy + '-' + MM + '-' + dd;
        }
        return date;
    }
}

// export default dataRules;
