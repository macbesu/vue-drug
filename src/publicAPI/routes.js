import Login from './../login'
import Home from './../home'

/*数据管理*/
import SampleSiteInfo from './../dataManage/sampleSiteInfo'
import SampleInfo from './../dataManage/sampleInfo'
import StrainBasicInfo from './../dataManage/strainBasicInfo'
import MicInfoYang from './../dataManage/micInfoYang'
import MicInfoYin from './../dataManage/micInfoYin'
import GeneBasicInfo from './../dataManage/geneBasicInfo'

/*数据管理——导入*/
import SamSiteImpot from './../dataManage/import/samSiteImpot'
import SamImpot from './../dataManage/import/samImpot'
import StrainImpotNo from './../dataManage/import/strainImpotNo'
import StrainImpotFen from './../dataManage/import/strainImpotFen'
import MicYangImpot from './../dataManage/import/micYangImpot'
import MicYinImpot from './../dataManage/import/micYinImpot'
import GeneImpot from './../dataManage/import/geneImpot'

/*数据汇总——分析界面*/
// import DetectionRate from './../dataSummary/detectionRate'
// import AnalyseDetec from './../dataSummary/analyseDetec'
import GeneJianchuRate from './../dataSummary/geneJianchuRate'
import AnalyseGeneResis from './../dataSummary/analyseGeneResis'
import DrugResistance from './../dataSummary/drugResistance'
import DrugResistYang from './../dataSummary/drugResistYang'
import DrugResistYin from './../dataSummary/drugResistYin'
import AnalyseDrugResis from './../dataSummary/analyseDrugResis'
import AnalyseDrugResisYang from './../dataSummary/analyseDrugResisYang'
import AnalyseDrugResisYin from './../dataSummary/analyseDrugResisYin'

/*数据汇总——表格*/
// import GeneJianchuRate from './../dataSummary/chart/geneJianchuRate'
import AnalysGene from './../dataSummary/chart/analysGene'
import AnlysAll from './../dataSummary/chart/anlysAll'
import AnlysYang from './../dataSummary/chart/anlysYang'
import AnlysYin from './../dataSummary/chart/anlysYin'

/*编号管理*/
import DrugInfo from './../numberManage/drugInfo'
import SourceNumber from './../numberManage/sourceNumber'
import AnimalNumber from './../numberManage/animalNumber'
import StrainNumber from './../numberManage/strainNumber'
import PartNumber from './../numberManage/partNumber'
import GeneInfo from './../numberManage/geneInfo'
import GeneSubType from './../numberManage/geneSubType'

/*设置*/
import UserList from './../systemManage/userList'
import AccessManage from './../systemManage/accessManage'
import HomeSetting from './../systemManage/homeSetting'

export default [
    {//登录
        path: '/',
        name: 'login',
        component: Login,
    },
    {//首页
        path: '/home',
        name: 'home',
        component: Home
    },

    {//采样地信息
        path: '/sampleSiteInfo',
        name: 'sampleSiteInfo',
        component: SampleSiteInfo
    },
    {//采样地信息——导入
        path: '/sampleSiteInfo/samSiteImpot',
        name: 'samSiteImpot',
        component: SamSiteImpot
    },

    {//样品信息
        path: '/sampleInfo',
        name: 'sampleInfo',
        component: SampleInfo
    },
    {//样品信息——导入
        path: '/sampleInfo/samImpot',
        name: 'samImpot',
        component: SamImpot
    },

    {//菌株基本信息
        path: '/strainBasicInfo',
        name: 'strainBasicInfo',
        component: StrainBasicInfo
    },
    {//菌株基本信息——导入编号
        path: '/strainBasicInfo/strainImpotNo',
        name: 'strainImpotNo',
        component: StrainImpotNo
    },
    {//菌株基本信息——导入分型信息
        path: '/strainBasicInfo/strainImpotFen',
        name: 'strainImpotFen',
        component: StrainImpotFen
    },

    {//MIC信息（阳性）
        path: '/micInfoYang',
        name: 'micInfoYang',
        component: MicInfoYang
    },
    {//MIC信息（阳性）——导入
        path: '/micInfoYang/micYangImpot',
        name: 'micYangImpot',
        component: MicYangImpot
    },
    {//MIC信息（阴性）
        path: '/micInfoYin',
        name: '/micInfoYin',
        component: MicInfoYin
    },
    {//MIC信息（阴性）——导入
        path: '/micInfoYin/micYinImpot',
        name: 'micYinImpot',
        component: MicYinImpot
    },
    {//耐药性基因信息
        path: '/geneBasicInfo',
        name: 'geneBasicInfo',
        component: GeneBasicInfo
    },
    {//MIC信息（阴性）——导入
        path: '/geneBasicInfo/geneImpot',
        name: 'geneImpot',
        component: GeneImpot
    },


    // {//检出率分析
    //     path: '/detectionRate',
    //     name: 'detectionRate',
    //     component: DetectionRate
    // },
    // {//检出率分析——分析界面
    //     path: '/analyseDetec',
    //     name: 'analyseDetec',
    //     component: AnalyseDetec
    // },
    // {//检出率分析——表格
    //     path: '/analyseDetec/jianchu',
    //     name: 'jianchu',
    //     component: Jianchu
    // },
    {//基因检出率分析
        path: '/geneJianchuRate',
        name: 'geneJianchuRate',
        component: GeneJianchuRate
    },
    {//基因检出率分析——分析界面
        path: '/analyseGeneResis',
        name: 'analyseGeneResis',
        component: AnalyseGeneResis
    },
    {//基因检出率分析——表格
        path: '/analysGeneResis/analyseGene',
        name: 'analysGene',
        component: AnalysGene
    },
    {//耐药性分析
        path: '/drugResistance',
        name: 'drugResistance',
        component: DrugResistance
    },
    {//耐药性分析——分析界面
        path: '/analyseDrugResis',
        name: 'analyseDrugResis',
        component: AnalyseDrugResis
    },
    {//耐药性分析——表格
        path: '/analyseDrugResis/anlysAll',
        name: 'anlysAll',
        component: AnlysAll
    },
    {//耐药性分析——阳性
        path: '/drugResistYang',
        name: 'drugResistYang',
        component: DrugResistYang
    },
    {//耐药性分析——阳性——分析界面
        path: '/analyseDrugResisYang',
        name: 'analyseDrugResisYang',
        component: AnalyseDrugResisYang
    },
    {//耐药性分析——阳性——表格
        path: '/analyseDrugResisYang/anlysYang',
        name: 'anlysYang',
        component: AnlysYang
    },
    {//耐药性分析——阴性
        path: '/drugResistYin',
        name: 'drugResistYin',
        component: DrugResistYin
    },
    {//耐药性分析——阴性——分析界面
        path: '/analyseDrugResisYin',
        name: 'analyseDrugResisYin',
        component: AnalyseDrugResisYin
    },
    {//耐药性分析——阴性——表格
        path: '/analyseDrugResisYin/anlysYin',
        name: 'anlysYin',
        component: AnlysYin
    },



    {//药品标准信息
        path: '/drugInfo',
        name: 'drugInfo',
        component: DrugInfo
    },
    {//来源编号
        path: '/sourceNumber',
        name: 'sourceNumber',
        component: SourceNumber
    },
    {//采样动物编号
        path: '/animalNumber',
        name: 'animalNumber',
        component: AnimalNumber
    },
    {//菌种编号
        path: '/strainNumber',
        name: 'strainNumber',
        component: StrainNumber
    },
    {//采集部位编号
        path: '/partNumber',
        name: 'partNumber',
        component: PartNumber
    },
    {// 基因亚型管理
        path: '/geneSubType',
        name: 'geneSubType',
        component: GeneSubType
    },
    {// 基因管理
        path: '/geneInfo',
        name: 'geneInfo',
        component: GeneInfo
    },



    {//用户列表
        path: '/userList',
        name: 'userList',
        component: UserList
    },
    {//权限管理
        path: '/accessManage',
        name: 'accessManage',
        component: AccessManage
    },
    {//主页设置
        path: '/homeSetting',
        name: 'homeSetting',
        component: HomeSetting
    },
    {//重定向到——首页
        path: '*', redirect: '/'
    }
]
